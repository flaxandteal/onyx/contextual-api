FROM golang:1.17-alpine as chef
WORKDIR /app

# Install dependencies - this is the caching Docker layer
COPY go.mod .
COPY go.sum .
RUN go mod download

FROM chef as builder
COPY . .
RUN go build -o dp-nlp-alpha-hub

# We do not need the Go toolchain to serve the compiled binary
FROM alpine
WORKDIR /app
COPY --from=builder /app/dp-nlp-alpha-hub .
ENTRYPOINT ["./dp-nlp-alpha-hub"]
