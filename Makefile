SHELL=bash
MAIN=dp-nlp-alpha-hub
BERLINFILE=berlin-search-schema.json
SCRUBBERFILE=scrubber-search-schema.json
CATEGORYFILE=category-search-schema.json

GOCMD=go

GREEN  := $(shell tput -Txterm setaf 2)
YELLOW := $(shell tput -Txterm setaf 3)
WHITE  := $(shell tput -Txterm setaf 7)
CYAN   := $(shell tput -Txterm setaf 6)
RESET  := $(shell tput -Txterm sgr0)

gen-berlin-types: ## creates berlin struct types from a json schema make sure link is available
	wget http://localhost:3001/berlin/search-schema -O $(BERLINFILE)
	gojsonschema -p payloads $(BERLINFILE) > payloads/berlin_struct.go
	rm $(BERLINFILE)

gen-scrubber-types: ## creates scrubber struct types from a json schema make sure link is available
	wget http://localhost:3002/scrubber/json-schema -O $(SCRUBBERFILE)
	gojsonschema -p payloads $(SCRUBBERFILE) > payloads/scrubber_struct.go
	rm $(SCRUBBERFILE)

gen-category-types: ## creates category struct types from a json schema make sure link is available
	wget http://localhost:3003/category/search-schema -O $(CATEGORYFILE)
	gojsonschema -p payloads $(CATEGORYFILE) > payloads/category_struct.go
	rm $(CATEGORYFILE)

build: ## build main.go => bin/dp-nlp-alpha-hub
	go build -o bin/dp-nlp-alpha-hub main.go

run: ## runs main.go
	go run main.go

compile: ## compiles to freebsd, linux and windows
	@echo Compiling for every OS and Platform
	GOOS=freebsd GOARCH=386 go build -o bin/dp-nlp-alpha-hub-freebsd-386 main.go
	GOOS=linux GOARCH=386 go build -o bin/dp-nlp-alpha-hub-linux-386 main.go
	GOOS=windows GOARCH=386 go build -o bin/dp-nlp-alpha-hub-windows-386 main.go

download: ## Installs all go dependencies
	@echo Installing all dependencies
	go mod download

clean: ## remove /bin folder
	rm -fr ./bin
	rm -fr ./vendor

help: ## Show this help.
	@echo ''
	@echo 'Usage:'
	@echo '  ${YELLOW}make${RESET} ${GREEN}<target>${RESET}'
	@echo ''
	@echo 'Targets:'
	@awk 'BEGIN {FS = ":.*?## "} { \
		if (/^[a-zA-Z_-]+:.*?##.*$$/) {printf "    ${YELLOW}%-20s${GREEN}%s${RESET}\n", $$1, $$2} \
		else if (/^## .*$$/) {printf "  ${CYAN}%s${RESET}\n", substr($$1,4)} \
		}' $(MAKEFILE_LIST)