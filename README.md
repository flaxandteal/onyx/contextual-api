# dp-nlp-alpha-hub

A Go application microservice to consolidate enhanced search functionality. 
In particular, this will call out to the dedicated thin wrappers for ML models where required.

### Getting started

Set up dependencies locally using either:
* make download
* go mod download

### Dependencies

You will require:

* berlin api default port is: 3001
* scrubber api default port is: 3002
* category-server api default port is: 3003

You can set them either locally or in a cluster. 
You can set the urls/ports using the .env.example file

### Logging
Loggin is setup as per ONS loggin standardts listed here:
https://github.com/ONSdigital/dp/blob/main/standards/LOGGING_STANDARDS.md

### Configuration

An overview of the configuration options available, either as a table of
environment variables, or with a link to a configuration guide.

| Environment variable | Default | Description
| -------------------- | ------- | -----------
| APP_ADDR             | :4000                   | The host and port to bind to
| APP_BERLINBASE       | "http://localhost:3001" | The default host and port of berlin
| APP_SCRUBBERBASE     | "http://localhost:3002" | The default host and port of scrubber
| APP_CATEGORYBASE	   | "http://localhost:3003" | The default host and port of category

### License

Prepared by Flax & Teal Limited for ONS Alpha project.
Copyright © 2022, Office for National Statistics (https://www.ons.gov.uk)

Released under MIT license, see [LICENSE](LICENSE.md) for details.
