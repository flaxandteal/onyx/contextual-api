package config

import (
	"context"
	"fmt"

	"github.com/ONSdigital/log.go/v2/log"
	"github.com/joho/godotenv"
	"github.com/kelseyhightower/envconfig"
)

type Config struct {
	Addr         string
	ScrubberBase string
	BerlinBase   string
	CategoryBase string
}

// Get configures the application and returns the Config
func GetCfg(ctx context.Context) *Config {
	cfg := &Config{}

	if err := godotenv.Load(); err != nil {
		log.Error(ctx, "error loading .env file: %s", err)
	}

	if err := envconfig.Process("APP", cfg); err != nil {
		log.Error(ctx, "error processing env variables", err)
	}

	if cfg.Addr != "" {
		return cfg
	}

	cfg = &Config{
		Addr:         ":5000",
		ScrubberBase: "http://search-scrubber:3002/scrubber/search",
		BerlinBase:   "http://berlin:3001/berlin/search",
		CategoryBase: "http://ff-fasttext-api:3003/category/search",
	}

	return cfg
}

func (c *Config) Info(ctx context.Context) {
	log.Info(ctx, fmt.Sprintf("Address is set to: %s", c.Addr))
	log.Info(ctx, fmt.Sprintf("Scrubber is set to: %s", c.ScrubberBase))
	log.Info(ctx, fmt.Sprintf("Berlin is set to: %s", c.BerlinBase))
	log.Info(ctx, fmt.Sprintf("Category is set to: %s", c.CategoryBase))
}
