package handlers

import (
	"encoding/json"
	"net/http"

	"gitlab/flaxandteal/Onyx/dp-nlp-alpha-hub/config"
	"gitlab/flaxandteal/Onyx/dp-nlp-alpha-hub/controller"
	"gitlab/flaxandteal/Onyx/dp-nlp-alpha-hub/params"
	"gitlab/flaxandteal/Onyx/dp-nlp-alpha-hub/payloads"
)

func CallOnyxHub(cfg *config.Config) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)

		ctx := r.Context()

		var result payloads.AlphaHub

		// Gets the scrubber response
		controller.MakeRequest(ctx, cfg.ScrubberBase, params.GetScrubberParams(r.URL.Query()), &result.Scrubber)

		// Gets the berlin response using a filter from url params and a query from scrubber
		controller.MakeRequest(ctx, cfg.BerlinBase, params.GetBerlinParams(result.Scrubber.Query, *params.GetFilters(r.URL.Query())), &result.Berlin)

		// Gets the category response using berlin normalized query
		controller.MakeRequest(ctx, cfg.CategoryBase, params.GetCategoryParams(result.Berlin.Query.Normalized), &result.Category)

		// This is here for testing purposes
		allResponses, err := json.Marshal(result)
		if err != nil {
			w.Write([]byte("issue with marshaling"))
			return
		}

		w.Write([]byte(allResponses))
	}
}

func HealthCheck(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)

	w.Write([]byte("ok"))
}
