package main

import (
	"context"
	"fmt"
	"gitlab/flaxandteal/Onyx/dp-nlp-alpha-hub/config"
	"gitlab/flaxandteal/Onyx/dp-nlp-alpha-hub/handlers"

	"github.com/ONSdigital/log.go/v2/log"

	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	log.Namespace = "dp-nlp-alpha-hub-api"
	// where do i put the context.Background?
	ctx := context.Background()

	cfg := config.GetCfg(ctx)

	cfg.Info(ctx)

	r := mux.NewRouter()

	routes(r, cfg)
	srv := &http.Server{
		Addr:    cfg.Addr,
		Handler: r,
	}

	log.Info(ctx, fmt.Sprintf("Starting server on %s", cfg.Addr))
	log.Fatal(ctx, "Server stopped: ", srv.ListenAndServe())
}

func routes(r *mux.Router, cfg *config.Config) {

	r.Path("/search").
		Queries("q", "{q}").
		HandlerFunc(handlers.CallOnyxHub(cfg)).
		Name("Show")

	r.Path("/search").
		Queries("state", "{state}").
		HandlerFunc(handlers.CallOnyxHub(cfg)).
		Name("Show")

	r.Path("/health").
		HandlerFunc(handlers.HealthCheck).
		Name("Health")
}
