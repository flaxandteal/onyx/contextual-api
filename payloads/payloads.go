package payloads

type AlphaHub struct {
	Scrubber ScrubberSearchSchemaJson
	Category Category
	Berlin   BerlinSearchSchemaJson
}
